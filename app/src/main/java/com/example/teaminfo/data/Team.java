package com.example.teaminfo.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Team {
    public String name,img, file,league;
    public ArrayList<Pair<String, String>> overview = new ArrayList<>();
    public ArrayList<Pair<String, String>> venue = new ArrayList<>();
    public ArrayList<Pair<String, String>> staff = new ArrayList<>();
    public ArrayList<Pair<String, String>> corporate = new ArrayList<>();
    public ArrayList<Game> history = new ArrayList<>();
    public ArrayList<Player> players = new ArrayList<>();



    public Team(JSONObject object) throws JSONException {
        name = object.getString("team");
        img = object.getString("img");
        file = object.getString("file");
    }

    public void addData(JSONObject object) throws JSONException {
        this.league = object.getString("league");
        JSONArray owerview = object.getJSONArray("overview");
        for(int i = 0; i < owerview.length(); i++) {
            String name = owerview.getJSONObject(i).getString("name");
            String value = owerview.getJSONObject(i).getString("value");
            this.overview.add(new Pair<>(name,value));
        }
        JSONArray venue = object.getJSONArray("venue");
        for(int i = 0; i < venue.length(); i++) {
            String name = venue.getJSONObject(i).getString("name");
            String value = venue.getJSONObject(i).getString("value");
            this.venue.add(new Pair<>(name,value));
        }
        JSONArray staff = object.getJSONArray("staff");
        for(int i = 0; i < staff.length(); i++) {
            String name = staff.getJSONObject(i).getString("name");
            String value = staff.getJSONObject(i).getString("value");
            this.staff.add(new Pair<>(name,value));
        }
        JSONArray corporate = object.getJSONArray("corporate");
        for(int i = 0; i < corporate.length(); i++) {
            String name = corporate.getJSONObject(i).getString("name");
            String value = corporate.getJSONObject(i).getString("value");
            this.corporate.add(new Pair<>(name,value));
        }
        JSONArray history = object.getJSONArray("history");
        for(int i = 0; i < history.length(); i++) {
            Game game = new Game(history.getJSONObject(i));
            this.history.add(game);
        }
        JSONArray players = object.getJSONArray("players");
        for(int i = 0; i < players.length(); i++) {
            Player player = new Player(players.getJSONObject(i));
            this.players.add(player);
        }
    }

    public void clearData() {
        overview = new ArrayList<>();
        venue = new ArrayList<>();
        staff = new ArrayList<>();
        corporate = new ArrayList<>();
        history = new ArrayList<>();
        players = new ArrayList<>();
    }


}
