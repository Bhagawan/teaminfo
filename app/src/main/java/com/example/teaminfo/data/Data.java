package com.example.teaminfo.data;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Data {
    public static ArrayList<Team> data = new ArrayList<>();

    public static void addData(JSONArray array) throws JSONException {
        for(int i = 0; i < array.length(); i++) {
            data.add(new Team(array.getJSONObject(i)));
        }
    }
}
