package com.example.teaminfo.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Player {
    public String name, face, position, country, flag;

    public Player(JSONObject object) throws JSONException {
        this.face = object.getString("face");
        this.position = object.getString("position");
        this.country = object.getString("country");
        this.flag = object.getString("flag");
        this.name = object.getString("name");
    }
}
