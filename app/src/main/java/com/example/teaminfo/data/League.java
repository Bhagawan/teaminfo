package com.example.teaminfo.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class League {
    public String lgFirst, lgSecond, leagueName;
    public ArrayList<LeagueTeam> standings = new ArrayList<>();

    public League(JSONObject object) throws JSONException {
        this.lgFirst = object.getString("one");
        this.lgSecond = object.getString("two");
        this.leagueName = object.getString("leaguename");
        JSONArray arr = object.getJSONArray("league");
        for(int i = 0; i < arr.length(); i++) {
            this.standings.add(new LeagueTeam(arr.getJSONObject(i)));
        }
        Collections.sort(standings,new PlaceSorter());
    }

    public String getLogo(String string) {
        for(LeagueTeam team: standings) {
            if (team.name.equals(string)) return team.img;
        }
        return null;
    }

    public class PlaceSorter implements Comparator<LeagueTeam>
    {
        @Override
        public int compare(LeagueTeam t1, LeagueTeam t2) {
            return Integer.compare(t1.place, t2.place);
        }
    }

}
