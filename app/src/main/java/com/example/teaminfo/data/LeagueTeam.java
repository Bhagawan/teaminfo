package com.example.teaminfo.data;

import org.json.JSONException;
import org.json.JSONObject;

public class LeagueTeam {
    public int place, games, win, draw, lose, diff, pts;
    public String name, img;

    public LeagueTeam(JSONObject object) throws JSONException {
        this.diff = object.getInt("diff");
        this.place = object.getInt("place");
        this.games = object.getInt("games");
        this.win = object.getInt("win");
        this.lose = object.getInt("lose");
        this.draw = object.getInt("draw");
        this.pts = object.getInt("pts");
        this.name = object.getString("name");
        this.img = object.getString("img");
    }

    public int getPlace() {
        return place;
    }
}
