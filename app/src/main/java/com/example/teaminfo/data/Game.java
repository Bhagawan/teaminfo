package com.example.teaminfo.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Game {
    public String opponent, img, league, gameData;
    public int endOppPt = -1, endTeamPt = -1;

    public Game(JSONObject object) throws JSONException {
        this.opponent = object.getString("opponent");
        this.img = object.getString("img");
        this.league = object.getString("league");
        this.gameData = object.getString("data");
        String oppt =object.getString("endOppPts");
        String tpt =object.getString("endTeamPts");
        if(!oppt.equals(""))
            this.endOppPt = Integer.parseInt(oppt);
        if(!tpt.equals(""))
            this.endTeamPt = Integer.parseInt(tpt);
    }
}
