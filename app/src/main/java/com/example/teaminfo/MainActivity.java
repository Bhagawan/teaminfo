package com.example.teaminfo;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.teaminfo.data.Team;
import com.example.teaminfo.util.MainMenuCardAdapter;

import java.util.ArrayList;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class MainActivity extends MvpAppCompatActivity implements MainMenuViewInterface{

    @InjectPresenter
    MainMenuPresenter presenter;

    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fullscreen();
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void fillMenu(ArrayList<Team> array) {
        RecyclerView recyclerView = findViewById(R.id.menu_recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        MainMenuCardAdapter adapter = new MainMenuCardAdapter(this, array);
        adapter.setClickListener((view, position) -> {
            Intent intent = new Intent(MainActivity.this, TeamActivity.class);
            intent.putExtra("pos",position);
            startActivity(intent);
        });
        recyclerView.setAdapter(adapter);
    }
}