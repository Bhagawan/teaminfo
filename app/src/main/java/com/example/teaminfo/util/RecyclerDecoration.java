package com.example.teaminfo.util;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    public RecyclerDecoration(Drawable divider) {
        mDivider = divider;
    }
    public RecyclerDecoration() {

    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getChildAdapterPosition(view) == 0) {
            return;
        }

        //outRect.top = mDivider.getIntrinsicHeight();
        outRect.top = 2;
    }

    @Override
    public void onDraw(@NonNull Canvas canvas, RecyclerView parent, @NonNull RecyclerView.State state) {
        int dividerLeft = parent.getPaddingLeft() + 10;
        int dividerRight = parent.getWidth() - parent.getPaddingRight() - 10;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 1; i+=2) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int dividerTop = child.getBottom() + params.bottomMargin;
            int dividerBottom = dividerTop + 2;
//            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();
//
//            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
//            mDivider.draw(canvas);
            Paint paint = new Paint();
            paint.setColor(Color.parseColor("#9C9C9C"));
            canvas.drawRect(dividerLeft, dividerTop, dividerRight, dividerBottom,paint);
            //canvas.drawColor(Color.parseColor("#9C9C9C"));
        }
    }
}
