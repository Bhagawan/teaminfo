package com.example.teaminfo.util;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.Game;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HistoryRAdapter  extends RecyclerView.Adapter<HistoryRAdapter.ViewHolder>{
    private ArrayList<Game> array;
    private boolean isFuture;

    public HistoryRAdapter(ArrayList<Game> array, boolean isFuture) {
        this.isFuture = isFuture;
        this.array = array;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView opponent, league, data;
        TextView status;
        ImageView logo;
        public ViewHolder(View itemView) {
            super(itemView);
            opponent = itemView.findViewById(R.id.game_opponent_name);
            league = itemView.findViewById(R.id.game_opponent_league);
            data = itemView.findViewById(R.id.game_data);
            logo = itemView.findViewById(R.id.game_opponent_logo);
            status = itemView.findViewById(R.id.game_status);
        }
    }

    @NonNull
    @Override
    public HistoryRAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_card, parent, false);
        return new HistoryRAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HistoryRAdapter.ViewHolder holder, int position) {
        Game game  = array.get(position);
        holder.opponent.setText(game.opponent);
        holder.league.setText(game.league);
        holder.data.setText(game.gameData);
        Picasso.get().load(game.img).resize(70, 70).into(holder.logo);

        if(isFuture) {
            holder.status.setCompoundDrawablesWithIntrinsicBounds(null,null,holder.data.getContext().getResources().getDrawable(R.drawable.ic_baseline_tv_24),null);
        }
        else {
            holder.status.setTextSize(15);
            holder.status.setTypeface(Typeface.DEFAULT_BOLD);
            if(game.endTeamPt != -1 && game.endOppPt != -1) {
                if(game.endTeamPt > game.endOppPt) {
                    holder.status.setText(game.endTeamPt + "-" + game.endOppPt);
                    holder.status.setTextColor(holder.data.getContext().getResources().getColor(R.color.green));
                }
                else {
                    holder.status.setText(game.endOppPt + "-" + game.endTeamPt);
                    holder.status.setTextColor(holder.data.getContext().getResources().getColor(R.color.red));
                }
            }
            else holder.status.setText("Не обновлено");
        }
    }

    @Override
    public int getItemCount() {
        if(array.isEmpty()) return 0;
        else return array.size();
    }
}
