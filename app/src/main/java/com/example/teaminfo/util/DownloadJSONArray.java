package com.example.teaminfo.util;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadJSONArray extends AsyncTask<String, Void, JSONArray> {

    public interface Result {
        void handleResult(JSONArray array) throws JSONException;
    }
    public Result resultInterface = null;

    @Override
    protected JSONArray doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream input = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            StringBuilder string = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                string.append(line).append('\n');
            }
            input.close();
            return new JSONArray(string.toString());
        } catch ( JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(JSONArray array) {
        try {
            if(array != null)
                resultInterface.handleResult(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
