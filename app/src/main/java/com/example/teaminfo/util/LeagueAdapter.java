package com.example.teaminfo.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.LeagueTeam;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LeagueAdapter extends RecyclerView.Adapter<LeagueAdapter.ViewHolder> {
    private ArrayList<LeagueTeam> league;

    public LeagueAdapter(ArrayList<LeagueTeam> league) {
        this.league = league;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView place, team, games, win, draw, lost, diff, pts;
        ImageView logo;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.diff = itemView.findViewById(R.id.league_row_diff);
            this.place = itemView.findViewById(R.id.league_row_position);
            this.team = itemView.findViewById(R.id.league_row_team);
            this.games = itemView.findViewById(R.id.league_row_games);
            this.win = itemView.findViewById(R.id.league_row_wins);
            this.draw = itemView.findViewById(R.id.league_row_draws);
            this.lost = itemView.findViewById(R.id.league_row_losses);
            this.pts = itemView.findViewById(R.id.league_row_pts);
            this.logo = itemView.findViewById(R.id.league_row_logo);
        }
    }
    @NonNull
    @Override
    public LeagueAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.league_recycler_row, parent, false);
        return new LeagueAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LeagueAdapter.ViewHolder holder, int position) {
        LeagueTeam team = league.get(position);
        holder.place.setText(String.valueOf(team.place));
        Picasso.get().load(team.img).resize(50, 50).into(holder.logo);
        holder.team.setText(team.name);
        holder.games.setText(String.valueOf(team.games));
        holder.win.setText(String.valueOf(team.win));
        holder.draw.setText(String.valueOf(team.draw));
        holder.lost.setText(String.valueOf(team.lose));
        int d = team.diff;
        if (d > 0) holder.diff.setText("+" + d);
        else if (d == 0) holder.diff.setText(String.valueOf(d));
        else holder.diff.setText("-" + d);
        holder.pts.setText(String.valueOf(team.pts));
    }
    @Override
    public int getItemCount() {
        if(league.isEmpty()) return 0;
        else return league.size();
    }
}
