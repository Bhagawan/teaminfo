package com.example.teaminfo.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.Player;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.ViewHolder>{
    private ArrayList<Player> players;

    public PlayerAdapter(ArrayList<Player> players) {
        this.players = players;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView name, position, country;
        ImageView face, flag;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.player_card_name);
            this.position = itemView.findViewById(R.id.player_card_position);
            this.country = itemView.findViewById(R.id.player_card_country);
            this.face = itemView.findViewById(R.id.player_card_face);
            this.flag = itemView.findViewById(R.id.player_card_flag);
        }
    }

    @NonNull
    @Override
    public PlayerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_card, parent, false);
        return new PlayerAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerAdapter.ViewHolder holder, int position) {
        Player player = players.get(position);
        holder.name.setText(player.name);
        Picasso.get().load(player.face).resize(75, 75).into(holder.face);
        Picasso.get().load(player.flag).resize(30, 20).into(holder.flag);
        holder.position.setText(player.position);
        holder.country.setText(player.country);
    }
    @Override
    public int getItemCount() {
        if(players.isEmpty()) return 0;
        else return players.size();
    }

}
