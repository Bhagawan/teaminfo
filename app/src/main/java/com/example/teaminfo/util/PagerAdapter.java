package com.example.teaminfo.util;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.teaminfo.fragments.FixturesFragment;
import com.example.teaminfo.fragments.HomeFragment;
import com.example.teaminfo.fragments.LeagueFragment;
import com.example.teaminfo.fragments.PlayersFragment;
import com.example.teaminfo.fragments.ResultsFragment;


public class PagerAdapter extends FragmentStateAdapter {
    private int i;

    public PagerAdapter(@NonNull FragmentActivity fragmentActivity, int i) {
        super(fragmentActivity);
        this.i = i;
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch(position) {
            case 0: return new HomeFragment(i);
            case 1: return new FixturesFragment(i);
            case 2: return new ResultsFragment(i);
            case 3: return new LeagueFragment(i);
            case 4: return new PlayersFragment(i);
        }
        return new LeagueFragment(i);
    }

    @Override
    public int getItemCount() {
        return 5;
    }



}
