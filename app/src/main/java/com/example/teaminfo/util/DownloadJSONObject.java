package com.example.teaminfo.util;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadJSONObject extends AsyncTask<String, Void, JSONObject> {

        public interface TaskResult {
            void handleResult(JSONObject object) throws JSONException;
        }
        public DownloadJSONObject.TaskResult resultInterface = null;

        @Override
        protected JSONObject doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream input = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            StringBuilder string = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                string.append(line).append('\n');
            }
            input.close();
            return new JSONObject(string.toString());
        } catch ( JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

        protected void onPostExecute(JSONObject object) {
        try {
            if(object != null)
                resultInterface.handleResult(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    }