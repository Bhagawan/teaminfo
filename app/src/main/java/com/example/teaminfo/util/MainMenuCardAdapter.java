package com.example.teaminfo.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.Team;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainMenuCardAdapter extends RecyclerView.Adapter<MainMenuCardAdapter.Viewholder>{
    private ArrayList<Team> array;
    private Context context;
    private ItemClickListener mClickListener;


    public MainMenuCardAdapter(Context context, ArrayList<Team> array) {
        this.array = array;
        this.context = context;
    }

    public class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView logo;
        private TextView teamName;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.menu_card_teamlogo);
            teamName = itemView.findViewById(R.id.menu_card_teamname);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public MainMenuCardAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_card_layout, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainMenuCardAdapter.Viewholder holder, int position) {
        Team team = array.get(position);
        holder.teamName.setText(team.name);
        Picasso.get().load(team.img).resize(100, 100).into(holder.logo);
    }

    @Override
    public int getItemCount() {
        if(!array.isEmpty()) return array.size();
        return 0;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}
