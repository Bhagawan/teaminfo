package com.example.teaminfo;

import com.example.teaminfo.data.Team;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface MainMenuViewInterface  extends MvpView {

    @AddToEnd
    void showMessage(String message);

    @AddToEnd
    void fillMenu(ArrayList<Team> array);

}
