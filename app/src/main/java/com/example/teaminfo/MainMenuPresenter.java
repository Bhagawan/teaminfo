package com.example.teaminfo;

import com.example.teaminfo.data.Data;
import com.example.teaminfo.util.DownloadJSONArray;


import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class MainMenuPresenter extends MvpPresenter<MainMenuViewInterface>{

    @Override
    public void onFirstViewAttach() {
        downloadData();
    }



    private void downloadData() {
        DownloadJSONArray task = new DownloadJSONArray();
        task.resultInterface = array -> {
            Data.addData(array);
            if(!Data.data.isEmpty())
                getViewState().fillMenu(Data.data);
        };
        task.execute("http://116.202.108.46/TeamInfo/Teams.json");
    }
}
