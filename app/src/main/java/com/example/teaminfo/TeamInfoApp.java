package com.example.teaminfo;

import android.app.Application;

import com.onesignal.OneSignal;

public class TeamInfoApp extends Application {
    private static final String ONESIGNAL_APP_ID = "be5792b2-35a1-45f5-98de-e16ee7aef076";

    @Override
    public void onCreate() {
        super.onCreate();

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }
}
