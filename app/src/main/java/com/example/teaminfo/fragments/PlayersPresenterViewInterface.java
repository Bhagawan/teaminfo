package com.example.teaminfo.fragments;

import com.example.teaminfo.data.Player;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface PlayersPresenterViewInterface extends MvpView {
    @AddToEnd
    void fillRecycler(ArrayList<Player> array);
    @AddToEnd
    void showMessage(String message);

}
