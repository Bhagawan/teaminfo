package com.example.teaminfo.fragments;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teaminfo.R;
import com.example.teaminfo.data.League;
import com.example.teaminfo.util.LeagueAdapter;
import com.example.teaminfo.util.RecyclerDecoration;
import com.squareup.picasso.Picasso;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class LeagueFragment extends MvpAppCompatFragment implements LeaguePresenterViewInterface{
    private int pos;
    private View view;

    @InjectPresenter
    LeaguePresenter leaguePresenter;

    public LeagueFragment(int i) {pos = i;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_league, container, false);
        leaguePresenter.setLeague(pos);
        return view;
    }

    @Override
    public void fillRecycler(League league) {
        TextView firstTeam = view.findViewById(R.id.league_first_name);
        firstTeam.setText(league.lgFirst);
        ImageView firstLogo = view.findViewById(R.id.league_first_logo);
        Picasso.get().load(league.getLogo(league.lgFirst)).resize(50, 50).into(firstLogo);

        TextView secondTeam = view.findViewById(R.id.league_second_name);
        secondTeam.setText(league.lgSecond);
        ImageView secondLogo = view.findViewById(R.id.league_second_logo);
        Picasso.get().load(league.getLogo(league.lgSecond)).resize(50, 50).into(secondLogo);

        TextView legName = view.findViewById(R.id.league_header);
        legName.setText(league.leagueName);

        RecyclerView recyclerView = view.findViewById(R.id.league_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        LeagueAdapter adapter = new LeagueAdapter(league.standings);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemDecoration dividerItemDecoration = new RecyclerDecoration();
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}