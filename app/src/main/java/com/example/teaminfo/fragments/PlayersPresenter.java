package com.example.teaminfo.fragments;

import com.example.teaminfo.data.Data;
import com.example.teaminfo.util.DownloadJSONObject;


import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class PlayersPresenter extends MvpPresenter<PlayersPresenterViewInterface> {
    public PlayersPresenter() {}


    public void addPlayers(int num) {
        if(Data.data.get(num).players.isEmpty()) downloadPlayers(num);
        else getViewState().fillRecycler(Data.data.get(num).players);
    }

    public void downloadPlayers(int team) {
        Data.data.get(team).clearData();
        DownloadJSONObject task = new DownloadJSONObject();
        task.resultInterface = object -> {
            Data.data.get(team).addData(object);
            getViewState().fillRecycler(Data.data.get(team).players);
        };
        task.execute("http://116.202.108.46/TeamInfo/" + Data.data.get(team).file);
    }
}