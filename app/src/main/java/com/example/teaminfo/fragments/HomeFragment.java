package com.example.teaminfo.fragments;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teaminfo.R;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class HomeFragment extends MvpAppCompatFragment implements HomePresenterViewInterface{
    private View view;
    private int pos;

    @InjectPresenter
    HomePresenter Homepresenter;


    public HomeFragment(int i) {pos = i;}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        Homepresenter.setInfo(pos);
        return view;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addFact(int card, String name, String value) {
        @SuppressLint("InflateParams") TableRow tr = (TableRow) getLayoutInflater().inflate(R.layout.table_row, null);
        TextView nameText = tr.findViewById(R.id.name_tab_row);
        nameText.setText(name);
        TextView valueText = tr.findViewById(R.id.value_tab_row);
        valueText.setText(value);

        TableLayout tl;
        switch (card) {
            case 1:
                 tl = view.findViewById(R.id.overview_table);
                tl.addView(tr);
                break;
            case 2:
                tl = view.findViewById(R.id.venue_table);
                tl.addView(tr);
                break;
            case 3:
                tl = view.findViewById(R.id.staff_table);
                tl.addView(tr);
                break;
            case 4:
                tl = view.findViewById(R.id.corporate_table);
                tl.addView(tr);
                break;
        }
    }

    @Override
    public void clearField() {
        //view = getLayoutInflater().inflate(R.layout.fragment_home,container, false);

        TableRow underRow = view.findViewById(R.id.underline);
        TableRow tr = view.findViewById(R.id.overview_header);

        TableLayout tl = view.findViewById(R.id.overview_table);
        tl.removeAllViews();
        tl.addView(tr);
        tl.addView(underRow);

        TableRow underRow2 = view.findViewById(R.id.underline2);
        TableRow tr2 = view.findViewById(R.id.venue_header);
        TableLayout tl2 = view.findViewById(R.id.venue_table);
        tl2.removeAllViews();
        tl2.addView(tr2);
        tl2.addView(underRow2);

        TableRow underRow3 = view.findViewById(R.id.underline3);
        TableRow tr3 = view.findViewById(R.id.staff_header);
        TableLayout tl3 = view.findViewById(R.id.staff_table);
        tl3.removeAllViews();
        tl3.addView(tr3);
        tl3.addView(underRow3);

        TableRow underRow4 = view.findViewById(R.id.underline4);
        TableRow tr4 = view.findViewById(R.id.corporate_header);
        TableLayout tl4 = view.findViewById(R.id.corporate_table);
        tl4.removeAllViews();
        tl4.addView(tr4);
        tl4.addView(underRow4);
    }

    @Override
    public void setBitmap(Bitmap flag) {
        ImageView imageView = view.findViewById(R.id.card1_header_logo);
        imageView.setImageBitmap(flag);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Homepresenter.detachView(this);

    }

}