package com.example.teaminfo.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.example.teaminfo.data.Data;
import com.example.teaminfo.util.DownloadJSONObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomePresenterViewInterface> {
    private Bitmap logo;

    public HomePresenter() {}


    public void setInfo(int num) {
        if(Data.data.get(num).overview.isEmpty()) downloadData(num);
        else fillCards(num);
    }


    private void downloadData(int num) {
        Data.data.get(num).clearData();
        DownloadJSONObject task = new DownloadJSONObject();
        task.resultInterface = object -> {
            Data.data.get(num).addData(object);
            fillCards(num);
        };
        task.execute("http://116.202.108.46/TeamInfo/" + Data.data.get(num).file);
    }


    public void fillCards(int num) {
        getViewState().clearField();
        setFlag(num);
        for (int i = 0; i < Data.data.get(num).overview.size(); i++) {
            String name = Data.data.get(num).overview.get(i).getKey();
            String value = Data.data.get(num).overview.get(i).getValue();
            getViewState().addFact(1, name, value);
        }
        for (int i = 0; i < Data.data.get(num).venue.size(); i++) {
            String name = Data.data.get(num).venue.get(i).getKey();
            String value = Data.data.get(num).venue.get(i).getValue();
            getViewState().addFact(2, name, value);
        }
        for (int i = 0; i < Data.data.get(num).staff.size(); i++) {
            String name = Data.data.get(num).staff.get(i).getKey();
            String value = Data.data.get(num).staff.get(i).getValue();
            getViewState().addFact(3, name, value);
        }
        for (int i = 0; i < Data.data.get(num).corporate.size(); i++) {
            String name = Data.data.get(num).corporate.get(i).getKey();
            String value = Data.data.get(num).corporate.get(i).getValue();
            getViewState().addFact(4, name, value);
        }
    }

    public void setFlag(int num) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                logo = bitmap;
                getViewState().setBitmap(logo);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.get().load(Data.data.get(num).img).resize(40,40)
                .into(target);
    }


}
