package com.example.teaminfo.fragments;

import com.example.teaminfo.data.League;


import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface LeaguePresenterViewInterface extends MvpView {
    @AddToEnd
    void fillRecycler(League league);
    @AddToEnd
    void showMessage(String message);
}
