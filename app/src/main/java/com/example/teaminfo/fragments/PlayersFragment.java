package com.example.teaminfo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.Player;
import com.example.teaminfo.util.PlayerAdapter;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class PlayersFragment extends MvpAppCompatFragment implements PlayersPresenterViewInterface{
    private int pos;
    private View view;

    @InjectPresenter
    PlayersPresenter playersPresenter;

    public PlayersFragment(int i) {pos = i;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.players_layout, container, false);
        playersPresenter.addPlayers(pos);
        return view;
    }


    @Override
    public void fillRecycler(ArrayList<Player> array) {
        RecyclerView recyclerView = view.findViewById(R.id.players_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        PlayerAdapter adapter = new PlayerAdapter(array);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
