package com.example.teaminfo.fragments;

import com.example.teaminfo.data.Game;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface FixturesPresenterViewInterface extends MvpView {
    @AddToEnd
    void fillRecycler(ArrayList<Game> array);
    @AddToEnd
    void showMessage(String message);
}
