package com.example.teaminfo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.Game;
import com.example.teaminfo.util.HistoryRAdapter;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class ResultsFragment extends MvpAppCompatFragment implements ResultsPresenterViewInterface{
    private View view;
    private int pos;

    @InjectPresenter
    public ResultsPresenter ResPresenter;

    public ResultsFragment(int i) {pos = i;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fixtures_layout, container, false);
        ResPresenter.fillResults(pos);
        return view;
    }

    @Override
    public void onResume() {
        ResPresenter.fillResults(pos);
        super.onResume();
    }

    @Override
    public void fillRecycler(ArrayList<Game> array) {
        RecyclerView recyclerView = view.findViewById(R.id.fixtures_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        HistoryRAdapter adapter = new HistoryRAdapter(array,false);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ResPresenter.detachView(this);

    }
}
