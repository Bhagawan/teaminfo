package com.example.teaminfo.fragments;

import com.example.teaminfo.data.Data;
import com.example.teaminfo.data.Game;
import com.example.teaminfo.util.DownloadJSONObject;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class FixturesPresenter extends MvpPresenter<FixturesPresenterViewInterface> {

    public FixturesPresenter() {}

    @Override
    public void onFirstViewAttach() {
        //getViewState().fillRecycler(Data.data.get(0).history);
    }

    public void fillFixtures(int team) {
        ArrayList<Game> arrayList = new ArrayList<>();

        try {
            if(!Data.data.get(team).history.isEmpty()){
                for(int i = 0; i < Data.data.get(team).history.size(); i++) {
                    String date = Data.data.get(team).history.get(i).gameData;

                    Locale myLocale = new Locale("ru","RU");
                    SimpleDateFormat curFormatter = new SimpleDateFormat("dd.MM.yy, HH:mm", myLocale);
                    Date matchDate = curFormatter.parse(date);

                    assert matchDate != null;
                    if(!matchDate.before(new Date())) {
                        arrayList.add(Data.data.get(team).history.get(i));
                    }
                }
            }
            else downloadData(team);
        }
        catch (ParseException e) {
            getViewState().showMessage("Неверно заполнен элемент базы данных.(Дата)");
        }
        getViewState().fillRecycler(arrayList);
    }

    private void downloadData(int num) {
        Data.data.get(num).clearData();
        DownloadJSONObject task = new DownloadJSONObject();
        task.resultInterface = object -> {
            Data.data.get(num).addData(object);
            fillFixtures(num);
        };
        task.execute("http://116.202.108.46/TeamInfo/" + Data.data.get(num).file);
    }

}
