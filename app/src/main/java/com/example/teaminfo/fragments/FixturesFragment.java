package com.example.teaminfo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teaminfo.R;
import com.example.teaminfo.data.Game;
import com.example.teaminfo.util.HistoryRAdapter;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class FixturesFragment extends MvpAppCompatFragment implements FixturesPresenterViewInterface{
    private View view;
    private int pos;

    @InjectPresenter
    public FixturesPresenter FixPresenter;

    public FixturesFragment(int i) {pos = i;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fixtures_layout, container, false);
        FixPresenter.fillFixtures(pos);
        return view;
    }

//    @Override
//    public void onResume() {
//        FixPresenter.fillFixtures(pos);
//        super.onResume();
//    }

    @Override
    public void fillRecycler(ArrayList<Game> array) {
        RecyclerView recyclerView = view.findViewById(R.id.fixtures_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        HistoryRAdapter adapter = new HistoryRAdapter(array,true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FixPresenter.detachView(this);

    }
}
