package com.example.teaminfo.fragments;

import com.example.teaminfo.data.Data;
import com.example.teaminfo.data.League;
import com.example.teaminfo.util.DownloadJSONObject;


import moxy.InjectViewState;
import moxy.MvpPresenter;
@InjectViewState
public class LeaguePresenter extends MvpPresenter<LeaguePresenterViewInterface> {
    public LeaguePresenter() {}

    public void setLeague(int team) {
        DownloadJSONObject task = new DownloadJSONObject();
        task.resultInterface = object -> getViewState().fillRecycler(new League(object));
        task.execute("http://116.202.108.46/TeamInfo/" + Data.data.get(team).league);
    }
}
