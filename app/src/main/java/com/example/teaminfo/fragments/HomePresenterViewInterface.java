package com.example.teaminfo.fragments;

import android.graphics.Bitmap;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface HomePresenterViewInterface extends MvpView {
    @AddToEnd
    void showMessage(String message);
    @AddToEnd
    void addFact(int card, String name, String value);
    @AddToEnd
    void clearField();
    @AddToEnd
    void setBitmap(Bitmap flag);

}
